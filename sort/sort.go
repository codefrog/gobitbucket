package sort

import "bitbucket.org/codefrog/gobitbucket/filter"

type Sort struct {
	Direction Direction
	Filter    *filter.Filter
}
