package sort

type Direction bool

const (
	// Ascending Order Direction
	Ascending Direction = true

	// Descending Order Direction
	Descending Direction = false
)
