package gobitbucket

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

const BitbucketURL = "https://bitbucket.org/api/2.0"
const UserAgent = "de.codefrog.gobitbucket"

type Client struct {
	BaseURL *url.URL

	client *http.Client

	UserAgent string

	// user credentials
	username    string
	password    string
	accessToken string

	// services / apis
	Addon        *AddonService
	HookEvents   *HookEventsService
	Repositories *RepositoriesService
	Snippets     *SnippetsService
	Teams        *TeamsService
	User         *UserService
	Users        *UsersService
}

func newClient() *Client {
	httpClient := http.DefaultClient
	baseURL, _ := url.Parse(BitbucketURL)

	c := &Client{
		client:    httpClient,
		UserAgent: UserAgent,
		BaseURL:   baseURL,
	}

	c.Addon = &AddonService{client: c}
	c.HookEvents = &HookEventsService{client: c}
	c.Repositories = &RepositoriesService{client: c}
	c.Snippets = &SnippetsService{client: c}
	c.Teams = &TeamsService{client: c}
	c.User = &UserService{client: c}
	c.Users = &UsersService{client: c}

	return c
}

func NewClientWithBasicAuth(username string, password string) *Client {
	c := newClient()
	c.username = username
	c.password = password
	return c
}

func NewClientWithAccessToken(accessToken string) *Client {
	c := newClient()
	c.accessToken = accessToken
	return c
}

func (c *Client) NewRequest(method string, urlString string, body interface{}) (*http.Request, error) {
	path, err := url.Parse(urlString)
	if err != nil {
		return nil, err
	}

	targetURL := strings.TrimRight(c.BaseURL.String(), "/") + path.String()
	buf := new(bytes.Buffer)
	if body != nil {
		if str, ok := body.(string); ok {
			buf.WriteString(str)
		} else {
			err := json.NewEncoder(buf).Encode(body)
			if err != nil {
				return nil, err
			}
		}
	}

	req, err := http.NewRequest(method, targetURL, buf)
	if err != nil {
		return nil, err
	}

	if len(c.accessToken) > 0 {
		req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", c.accessToken))
	} else if len(c.username) > 0 && len(c.password) > 0 {
		req.SetBasicAuth(c.username, c.password)
	}

	req.Header.Add("User-Agent", c.UserAgent)
	return req, nil
}

func (c *Client) Do(req *http.Request, output interface{}) error {
	response, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	json.NewDecoder(response.Body).Decode(output)
	return nil
}
