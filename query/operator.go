package query

type operator string

const (
	equal                          operator = "="
	notEqual                       operator = "!="
	caseInsensitiveTextContains    operator = "~"
	caseInsensitiveTextNotContains operator = "!~"
	greaterThan                    operator = ">"
	greaterThanOrEqual             operator = ">="
	lessThan                       operator = "<"
	lessThanOrEqual                operator = "<="
)

type combineOperator string

const (
	and combineOperator = "AND"
	or  combineOperator = "OR"
)
