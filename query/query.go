package query

import (
	"fmt"
	"net/url"
	"strings"
)

type Query struct {
	Key      string
	Value    interface{}
	Operator operator

	subQueries      []*Query
	combineOperator combineOperator
}

func (q *Query) String() string {
	var result string
	if len(q.subQueries) > 0 {
		var queryStringSlice []string
		for _, query := range q.subQueries {
			queryStringSlice = append(queryStringSlice, query.String())
		}
		queryString := strings.Join(queryStringSlice, fmt.Sprintf("%s", q.combineOperator))
		result += fmt.Sprintf("(%s)", queryString)
	} else {
		result += fmt.Sprintf("(%s%s%s)", q.Key, q.Operator, url.QueryEscape(q.encodedValue()))
	}
	return result
}

func (q *Query) encodedValue() string {
	switch q.Value.(type) {
	case string:
		return fmt.Sprintf(`"%s"`, q.Value)
	default:
		return fmt.Sprintf("%v", q.Value)
	}
}

func newQuery(operator combineOperator, queries ...*Query) *Query {
	return &Query{
		subQueries:      queries,
		combineOperator: operator,
	}
}

func And(queries ...*Query) *Query {
	return newQuery(and, queries...)
}

func Or(queries ...*Query) *Query {
	return newQuery(or, queries...)
}

func Equals(Key string, Value interface{}) *Query {
	return &Query{
		Operator: equal,
		Key:      Key,
		Value:    Value,
	}
}

func NotEqual(Key string, Value interface{}) *Query {
	return &Query{
		Operator: notEqual,
		Key:      Key,
		Value:    Value,
	}
}

func CaseInsensitiveTextContains(Key string, Value interface{}) *Query {
	return &Query{
		Operator: caseInsensitiveTextContains,
		Key:      Key,
		Value:    Value,
	}
}

func CaseInsensitiveTextNotContains(Key string, Value interface{}) *Query {
	return &Query{
		Operator: caseInsensitiveTextNotContains,
		Key:      Key,
		Value:    Value,
	}
}

func GreaterThan(Key string, Value interface{}) *Query {
	return &Query{
		Operator: greaterThan,
		Key:      Key,
		Value:    Value,
	}
}

func GreaterThanOrEqual(Key string, Value interface{}) *Query {
	return &Query{
		Operator: greaterThanOrEqual,
		Key:      Key,
		Value:    Value,
	}
}

func LessThan(Key string, Value interface{}) *Query {
	return &Query{
		Operator: lessThan,
		Key:      Key,
		Value:    Value,
	}
}

func LessThanOrEqual(Key string, Value interface{}) *Query {
	return &Query{
		Operator: lessThanOrEqual,
		Key:      Key,
		Value:    Value,
	}
}
