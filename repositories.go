package gobitbucket

import (
	"bitbucket.org/codefrog/gobitbucket/filter"
	"fmt"
)

func (service *RepositoriesService) queryList(endPoint string, filter *filter.RepositoryFilter) ([]*Repository, error) {
	output := []*Repository{}
	count := 0
	page := 1

	for {
		queryEndPoint := fmt.Sprintf("%s?page=%d", endPoint, page)
		req, err := service.client.NewRequest("GET", queryEndPoint, nil)
		if err != nil {
			return nil, err
		}

		var repositories RepositoriesList
		err = service.client.Do(req, &repositories)
		if err != nil {
			return nil, err
		}

		// TOOD: is this required?
		repoValuesLength := len(repositories.Values)
		if repoValuesLength <= 0 {
			break
		}

		output = append(output, repositories.Values...)
		count += repoValuesLength

		if count >= repositories.Size {
			break
		}

		page++
	}

	return output, nil
}

func (service *RepositoriesService) List(username string, filter *filter.RepositoryFilter) ([]*Repository, error) {
	endPoint := fmt.Sprintf("/repositories/%s", username)
	return service.queryList(endPoint, filter)
}

func (service *RepositoriesService) Get(username string, repositoryName string) (*Repository, error) {
	endPoint := fmt.Sprintf("/repositories/%s/%s", username, repositoryName)
	req, err := service.client.NewRequest("GET", endPoint, nil)
	if err != nil {
		return nil, err
	}

	var repo Repository
	service.client.Do(req, &repo)

	return &repo, nil
}

func (service *RepositoriesService) Create(username string, repositoryName string, repository *Repository) (*Repository, error) {
	return nil, nil
}

func (service *RepositoriesService) Update(username string, repositoryName string, repository *Repository) (*Repository, error) {
	return nil, nil
}

func (service *RepositoriesService) Delete(username string, repositoryName string) (bool, error) {
	return false, nil
}
