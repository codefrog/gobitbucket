package filter

type RepositoryFilter struct {
	UUID        *Filter           `json:"uuid"`
	FullName    *Filter           `json:"full_name"`
	Scm         *Filter           `json:"scm"`
	Owner       *UserFilter       `json:"owner"`
	Name        *Filter           `json:"name"`
	Description *Filter           `json:"description"`
	CreatedOn   *Filter           `json:"created_on"`
	UpdatedOn   *Filter           `json:"updated_on"`
	Size        *Filter           `json:"size"`
	Language    *Filter           `json:"language"`
	Parent      *RepositoryFilter `json:"parent"`
	ForkPolicy  *Filter           `json:"fork_policy"`
	IsPrivate   *Filter           `json:"is_private"`
	HasIssues   *Filter           `json:"has_issues"`
	HasWiki     *Filter           `json:"has_wiki"`
}
