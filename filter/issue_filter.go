package filter

type IssueFilter struct {
	ID         *Filter     `json:"id"`
	Title      *Filter     `json:"title"`
	Reporter   *UserFilter `json:"reporter"`
	Assignee   *UserFilter `json:"assignee"`
	ContentRaw *Filter     `json:"content.raw"`
	CreatedOn  *Filter     `json:"created_on"`
	UpdatedOn  *Filter     `json:"updated_on"`
	State      *Filter     `json:"state"`
	Kind       *Filter     `json:"kind"`
	Priority   *Filter     `json:"priority"`
	Version    *Filter     `json:"version"`
	Component  *Filter     `json:"component"`
	Milestone  *Filter     `json:"milestone"`
	Watches    *Filter     `json:"watches"`
	Votes      *Filter     `json:"votes"`
}
