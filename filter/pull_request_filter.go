package filter

type PullRequestFilter struct {
	ID                    *Filter           `json:"id"`
	Title                 *Filter           `json:"title"`
	Description           *Filter           `json:"description"`
	Author                *UserFilter       `json:"author"`
	Reviewers             *UserFilter       `json:"reviewers"`
	State                 *Filter           `json:"state"`
	SourceRepository      *RepositoryFilter `json:"source.repository"`
	SourceBranchName      *Filter           `json:"source.branch.name"`
	DestinationRepository *RepositoryFilter `json:"destination.repository"`
	DestinationBranchName *Filter           `json:"destination.branch.name"`
	CloseSourceBranch     *Filter           `json:"close_source_branch"`
	ClosedBy              *UserFilter       `json:"closed_by"`
	Reason                *Filter           `json:"reason"`
	CreatedOn             *Filter           `json:"created_on"`
	UpdatedOn             *Filter           `json:"updated_on"`
	TaskCount             *Filter           `json:"task_count"`
	CommentCount          *Filter           `json:"comment_count"`
}
