package filter

import (
	"bitbucket.org/codefrog/gobitbucket/query"
	"encoding/json"
	"fmt"
	"net/url"
)

type Filter struct {
	Operator operator.Operator
	Value    interface{}
}

func NewFilter(operator operator.Operator, value interface{}) *Filter {
	return &Filter{
		Operator: operator,
		Value:    value,
	}
}

func (f *Filter) MarshalJSON() ([]byte, error) {
	return json.Marshal(fmt.Sprintf("%s%s", f.Operator, f.Value))
}

func (f *Filter) EncodeValues(key string, v *url.Values) error {
	//fmt.Println(key, v)
	if f.Value != nil {
		v.Add(key, fmt.Sprintf("%s%s", f.Operator, f.Value))
	}
	return nil
}
