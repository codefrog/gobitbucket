package utils

// Flatten takes a map and returns a new one where nested maps are replaced
// by dot-delimited keys.
// Thanks: https://stackoverflow.com/a/39625223/2830502
func Flatten(m map[string]interface{}) map[string]interface{} {
	o := make(map[string]interface{})
	for k, v := range m {
		switch child := v.(type) {
		case map[string]interface{}:
			nm := Flatten(child)
			for nk, nv := range nm {
				o[k+"."+nk] = nv
			}
		default:
			o[k] = v
		}
	}
	return o
}
