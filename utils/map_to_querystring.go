package utils

import (
	"fmt"
	//"net/url"
)

func MapToQueryString(m map[string]interface{}) string {
	var result string
	first := true
	for k, v := range m {
		if v != nil {
			if !first {
				result += "&"
			}
			//switch v.(type) {
			//case string:
			//	v = url.QueryEscape(v.(string))
			//	break
			//}
			result += fmt.Sprintf("%s=%s", k, v)
			first = false
		}
	}
	return result
}
