package utils

import "encoding/json"

func StructToMap(anyStruct interface{}) map[string]interface{} {
	var inInterface interface{}
	marshaledStruct, _ := json.Marshal(anyStruct)
	json.Unmarshal(marshaledStruct, &inInterface)
	return inInterface.(map[string]interface{})
}
