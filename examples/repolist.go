package main

import (
	"fmt"
	"log"

	"bitbucket.org/codefrog/gobitbucket"
	"bitbucket.org/codefrog/gobitbucket/filter"
	"bitbucket.org/codefrog/gobitbucket/operator"
)

func main() {
	c := gobitbucket.NewClientWithBasicAuth("", "")

	repos, err := c.Repositories.List("codefrog", &filter.RepositoryFilter{
		Name: filter.NewFilter(operator.CaseInsensitiveTextContains, "php"),
	})
	if err != nil {
		log.Fatal(err)
		return
	}

	fmt.Println("Repos Count", len(repos))
	for _, repo := range repos {
		fmt.Println(repo.Name)
	}
}
