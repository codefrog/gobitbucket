package main

import (
	"bitbucket.org/codefrog/gobitbucket/query"
	"fmt"
)

func main() {
	s := query.And(
		query.Equals("name", "php-roma"),
		query.Equals("name", "php-roma"),
		query.Equals("name", "php-roma"),
		query.Or(
			query.Equals("name", "php-roma"),
			query.Equals("name", "php-roma"),
			query.Equals("name", "php-roma"),
		),
	).String()
	fmt.Println(s)

	fmt.Println(query.Equals("name", "php-roma").String())

	//c.Repositories.ListBy("codefrog").
	//	Where(
	//		Field(gobitbucket.Repository.Name).Equals("php-roma"),
	//		Field(gobitbucket.Repository.Name).Equals("php-roma"),
	//	).
	//	Or().
	//	Where(
	//		Field(gobitbucket.Repository.Name).Equals("php-roma"),
	//		Field(gobitbucket.Repository.Name).Equals("php-roma"),
	//	)
	//
	//rsm := utils.StructToMap(r)
	//rsmf := utils.Flatten(rsm)
	//rsmfq := utils.MapToQueryString(rsmf)
	//
	//fmt.Println(rsm)
	//fmt.Println(rsmf)
	//fmt.Println(rsmfq)
	//fmt.Println(query.Values(rsm))
	//
	//j, _ := json.Marshal(rsm)
	//fmt.Println(string(j))

	//u, _ := query.Values(r)
	//fmt.Println(u.Encode())
	//
	//s, _ := json.Marshal("sadasd")
	//fmt.Println(string(s))
}
