package main

import (
	"fmt"
	"log"

	"bitbucket.org/codefrog/gobitbucket"
)

func main() {
	c := gobitbucket.NewClientWithBasicAuth("", "")

	repo, err := c.Repositories.Get("codefrog", "gobitbucket")
	if err != nil {
		log.Fatal(err)
		return
	}

	fmt.Println("Repo Scm:", repo.Scm)
	fmt.Println("Repo Owner:", repo.Owner)
	fmt.Println("Repo Name:", repo.Name)
	fmt.Println("Repo FullName:", repo.FullName)
	fmt.Println("Repo Description:", repo.Description)
	fmt.Println("Repo IsPrivate:", repo.IsPrivate)
	fmt.Println("Repo HasIssues:", repo.HasIssues)
	fmt.Println("Repo HasWiki:", repo.HasWiki)
	fmt.Println("Repo ForkPolicy:", repo.ForkPolicy)
	fmt.Println("Repo Language:", repo.Language)
	{
		fmt.Println("Repo Links Watchers:", repo.Links.Watchers.Href)
		fmt.Println("Repo Links Commits:", repo.Links.Commits.Href)
		fmt.Println("Repo Links Self:", repo.Links.Self.Href)
		fmt.Println("Repo Links HTML:", repo.Links.HTML.Href)
		fmt.Println("Repo Links Avatar:", repo.Links.Avatar.Href)
		fmt.Println("Repo Links Forks:", repo.Links.Forks.Href)
		{
			for _, link := range repo.Links.Clone {
				fmt.Println("Repo Links Clone:", link.Href)
			}
		}
		fmt.Println("Repo Links PullRequests:", repo.Links.PullRequests.Href)
	}
	fmt.Println("Repo Size:", repo.Size)
	fmt.Println("Repo CreatedOn:", repo.CreatedOn)
	fmt.Println("Repo UpdatedOn:", repo.UpdatedOn)
}
