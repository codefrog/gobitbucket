package gobitbucket

type AddonService struct {
	client *Client
}

type HookEventsService struct {
	client *Client
}

type RepositoriesService struct {
	client *Client
}

type SnippetsService struct {
	client *Client
}

type TeamsService struct {
	client *Client
}

type UserService struct {
	client *Client
}

type UsersService struct {
	client *Client
}
