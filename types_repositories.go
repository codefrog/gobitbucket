package gobitbucket

type RepositoriesList struct {
	PageLen int           `json:"pagelen"`
	Values  []*Repository `json:"values"`
	Page    int           `json:"page"`
	Size    int           `json:"size"`
}

type Repository struct {
	Scm         string          `json:"scm"`
	HasWiki     bool            `json:"has_wiki"`
	Description string          `json:"description"`
	Links       RepositoryLinks `json:"links"`
	ForkPolicy  string          `json:"fork_policy"`
	Language    string          `json:"language"`
	CreatedOn   string          `json:"created_on"`
	FullName    string          `json:"full_name"`
	HasIssues   bool            `json:"has_isses"`
	Owner       User            `json:"owner"`
	UpdatedOn   string          `json:"updated_on"`
	Size        int             `json:"size"`
	IsPrivate   bool            `json:"is_private"`
	Name        string          `json:"name"`
}

type RepositoryLinks struct {
	Watchers     Link        `json:"watchers"`
	Commits      Link        `json:"commits"`
	Self         Link        `json:"self"`
	HTML         Link        `json:"html"`
	Avatar       Link        `json:"avatar"`
	Forks        Link        `json:"fork"`
	Clone        []NamedLink `json:"clone"`
	PullRequests Link        `json:"pullrequests"`
}

type HookRepository struct {
	Website     string `json:"website"`
	Fork        bool   `json:"fork"`
	Name        string `json:"name"`
	Scm         string `json:"scm"`
	Owner       string `json:"owner"`
	AbsoluteURL string `json:"absolute_url"`
	Slug        string `json:"slug"`
	Private     bool   `json:"is_private"`
}
